package com.tmobile.standalone;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TMobileExecutorService {
    Set<String> duplicateList;
    List<HAProxyConfigModel> propList;
    List<String> finalList;
    public TMobileExecutorService(Set<String> duplicateList, List<HAProxyConfigModel> propList) {
        this.duplicateList = duplicateList;
        this.propList = propList;
    }

    public void execute()
    {
        List<String> mainList = new ArrayList<String>();
        mainList.addAll(duplicateList);

        List<List<String>> parts = new ArrayList<>();
        int L = 50;
        final int N = mainList.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<String>(mainList.subList(i, Math.min(N, i + L))));
        }
        System.out.println(parts.size());
        List<String> listFinal = new ArrayList<String>();
        ExecutorService service = Executors.newFixedThreadPool(5);
        // Or un fixed thread number
        // The number of threads will increase with tasks
        // ExecutorService service = Executors.newCachedThreadPool(10);
        for (List<String> partialKeysList : parts) {
            DuplicateKeysHelper helper = new DuplicateKeysHelper(partialKeysList,propList);
            service.execute(helper);
            try{
                Thread.sleep(5000);
            }catch(Exception es){}
            listFinal.addAll(helper.getNewPartialKeysList());
        }
        // shutdown
        // this will get blocked until all task finish
        setFinalList(listFinal);
        service.shutdown();
        System.out.println("In TMobileExecutorService listFinal.size():" + getFinalList().size());
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<String> getFinalList() {
        return finalList;
    }

    public void setFinalList(List<String> finalList) {
        this.finalList = finalList;
    }
}
