package com.tmobile.standalone;

public class TMobileFileParserException extends Exception {
    public TMobileFileParserException(String exception) {
        // Call constructor of parent Exception
        super(exception);
    }
}