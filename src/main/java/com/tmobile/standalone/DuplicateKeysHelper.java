package com.tmobile.standalone;

import java.util.ArrayList;
import java.util.List;

public class DuplicateKeysHelper implements Runnable {
        List<String> partialKeysList;
        List<HAProxyConfigModel> sourceList;
        List<String> newPartialKeysList = new ArrayList<>();
        public DuplicateKeysHelper(List<String> partialKeysList,List<HAProxyConfigModel> sourceList) {
            this.partialKeysList = partialKeysList;
            this.sourceList = sourceList;
        }

        @Override
        public void run() {
            // business logic at here
            for (String i1 : partialKeysList) {
                for (int i = 0; i < sourceList.size(); i++) {
                    if (i1.equals(sourceList.get(i).getKey())) {
                        newPartialKeysList.add(i1 + ":" + i);
                    }
                }
            }
            try{
                Thread.sleep(5000);
            }catch(Exception es){}
            setNewPartialKeysList(newPartialKeysList);
        }

    public List<String> getPartialKeysList() {
        return partialKeysList;
    }

    public void setPartialKeysList(List<String> partialKeysList) {
        this.partialKeysList = partialKeysList;
    }

    public List<HAProxyConfigModel> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<HAProxyConfigModel> sourceList) {
        this.sourceList = sourceList;
    }
    public void setNewPartialKeysList(List<String> newPartialKeysList) {
        this.newPartialKeysList = newPartialKeysList;
    }

    public List<String> getNewPartialKeysList() {
        return newPartialKeysList;
    }
}

