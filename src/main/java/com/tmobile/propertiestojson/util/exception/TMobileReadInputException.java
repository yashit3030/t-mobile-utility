package com.tmobile.propertiestojson.util.exception;

public class TMobileReadInputException extends RuntimeException {
    public TMobileReadInputException(Exception ex) {
        super(ex);
    }
}
