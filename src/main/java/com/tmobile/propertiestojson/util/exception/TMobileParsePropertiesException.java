package com.tmobile.propertiestojson.util.exception;


import com.tmobile.propertiestojson.resolvers.primitives.TMobileStringJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileStringToJsonTypeConverter;

public class TMobileParsePropertiesException extends RuntimeException {

    public static final String STRING_RESOLVER_AS_NOT_LAST = "Added some type resolver after " + TMobileStringJsonTypeResolver.class.getCanonicalName() +
                                                             ". This type resolver always should be last when is in configuration of resolvers";

    public static final String STRING__TO_JSON_RESOLVER_AS_NOT_LAST = "Added some type resolver after " + TMobileStringToJsonTypeConverter.class.getCanonicalName() +
                                                                      ". This type resolver always should be last when is in configuration of resolvers";

    public static final String PROPERTY_KEY_NEEDS_TO_BE_STRING_TYPE = "Unsupported property key type: %s for key: %s, Property key needs to be a string type";
    public static final String CANNOT_FIND_TYPE_RESOLVER_MSG = "Cannot find valid JSON type resolver for class: '%s'. \n" +
            "Please consider add sufficient resolver to your resolvers.";


    public static final String CANNOT_FIND_JSON_TYPE_OBJ = "Cannot find valid JSON type resolver for class: '%s'. \n" +
                                                               " for property: %s, and object value: %s \n" +
                                                               "Please consider add sufficient resolver to your resolvers.";
    public TMobileParsePropertiesException(String message) {
        super(message);
    }
}
