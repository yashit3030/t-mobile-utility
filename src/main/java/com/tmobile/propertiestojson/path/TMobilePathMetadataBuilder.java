package com.tmobile.propertiestojson.path;



import static com.tmobile.parser.utils.TMobileConstants.REGEX_DOT;

public class TMobilePathMetadataBuilder {

    public static TMobilePathMetadata createRootPathMetaData(String propertyKey) {
        String[] fields = propertyKey.split(REGEX_DOT);
        TMobilePathMetadata currentPathMetadata = null;

        for(int index = 0; index < fields.length; index++) {
            String field = fields[index];

            TMobilePathMetadata nextPathMetadata = new TMobilePathMetadata(propertyKey);
            nextPathMetadata.setParent(currentPathMetadata);
            nextPathMetadata.setFieldName(field);
            nextPathMetadata.setOriginalFieldName(field);

            if (currentPathMetadata != null) {
                currentPathMetadata.setChild(nextPathMetadata);
            }
            currentPathMetadata = nextPathMetadata;

        }
        return currentPathMetadata.getRoot();
    }
}
