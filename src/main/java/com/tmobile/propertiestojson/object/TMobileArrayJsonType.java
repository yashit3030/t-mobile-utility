package com.tmobile.propertiestojson.object;

import com.tmobile.propertiestojson.TMobilePropertyArrayHelper;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.util.exception.TMobileCannotOverrideFieldException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static com.tmobile.parser.utils.TMobileConstants.*;
import static com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType.*;
import static com.tmobile.propertiestojson.util.TMobileListUtil.getLastIndex;
import static com.tmobile.propertiestojson.util.TMobileListUtil.isLastIndex;


public class TMobileArrayJsonType extends TMobileAbstractJsonType implements TMobileMergableObject<TMobileArrayJsonType> {

    public static final int INIT_SIZE = 100;
    private TMobileAbstractJsonType[] elements = new TMobileAbstractJsonType[INIT_SIZE];
    private int maxIndex = -1;

    public TMobileArrayJsonType() {
    }

    public TMobileArrayJsonType(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, Collection<?> elements, TMobilePathMetadata currentPathMetadata, String propertyKey) {
        Iterator<?> iterator = elements.iterator();
        int index = 0;
        while(iterator.hasNext()) {
            Object element = iterator.next();
            addElement(index, primitiveJsonTypesResolver.resolvePrimitiveTypeAndReturn(element, propertyKey), currentPathMetadata);
            index++;
        }
    }

    public void addElement(int index, TMobileAbstractJsonType elementToAdd, TMobilePathMetadata currentPathMetadata) {
        if(maxIndex < index) {
            maxIndex = index;
        }
        rewriteArrayWhenIsFull(index);
        TMobileAbstractJsonType oldObject = elements[index];

        if(oldObject != null) {
            if(oldObject instanceof TMobileMergableObject && elementToAdd instanceof TMobileMergableObject) {
                TMobileMergableObject.mergeObjectIfPossible(oldObject, elementToAdd, currentPathMetadata);
            } else {
                throw new TMobileCannotOverrideFieldException(currentPathMetadata.getCurrentFullPath(), oldObject, currentPathMetadata.getOriginalPropertyKey());
            }
        } else {
            elements[index] = elementToAdd;
        }
    }

    public void addElement(TMobilePropertyArrayHelper propertyArrayHelper, TMobileAbstractJsonType elementToAdd, TMobilePathMetadata currentPathMetadata) {
        List<Integer> indexes = propertyArrayHelper.getDimensionalIndexes();
        int size = propertyArrayHelper.getDimensionalIndexes().size();
        TMobileArrayJsonType currentArray = this;
        for(int index = 0; index < size; index++) {
            if(isLastIndex(propertyArrayHelper.getDimensionalIndexes(), index)) {
                currentArray.addElement(indexes.get(index), elementToAdd, currentPathMetadata);
            } else {
                currentArray = createOrGetNextDimensionOfArray(currentArray, indexes, index, currentPathMetadata);
            }
        }
    }

    public static TMobileArrayJsonType createOrGetNextDimensionOfArray(TMobileArrayJsonType currentArray, List<Integer> indexes, int indexToTest, TMobilePathMetadata currentPathMetadata) {
        if(currentArray.existElementByGivenIndex(indexes.get(indexToTest))) {
            TMobileAbstractJsonType element = currentArray.getElement(indexes.get(indexToTest));
            if(element instanceof TMobileArrayJsonType) {
                return (TMobileArrayJsonType) element;
            } else {
                List<Integer> currentIndexes = indexes.subList(0, indexToTest + 1);
                String indexesAsText = currentIndexes.stream()
                                                     .map(Object::toString)
                                                     .reduce(EMPTY_STRING, (oldText, index) -> oldText + ARRAY_START_SIGN + index + ARRAY_END_SIGN);
                throw new TMobileCannotOverrideFieldException(currentPathMetadata.getCurrentFullPathWithoutIndexes() + indexesAsText, element, currentPathMetadata.getOriginalPropertyKey());
            }
        } else {
            TMobileArrayJsonType newArray = new TMobileArrayJsonType();
            currentArray.addElement(indexes.get(indexToTest), newArray, currentPathMetadata);
            return newArray;
        }
    }

    public TMobileAbstractJsonType getElementByGivenDimIndexes(TMobilePathMetadata currentPathMetaData) {
        TMobilePropertyArrayHelper propertyArrayHelper = currentPathMetaData.getPropertyArrayHelper();
        List<Integer> indexes = propertyArrayHelper.getDimensionalIndexes();
        int size = propertyArrayHelper.getDimensionalIndexes().size();
        TMobileArrayJsonType currentArray = this;
        for(int i = 0; i < size; i++) {
            if(isLastIndex(propertyArrayHelper.getDimensionalIndexes(), i)) {
                return currentArray.getElement(indexes.get(i));
            } else {
                TMobileAbstractJsonType element = currentArray.getElement(indexes.get(i));
                if(element == null) {
                    return null;
                }
                if(element instanceof TMobileArrayJsonType) {
                    currentArray = (TMobileArrayJsonType) element;
                } else {
                    List<Integer> currentIndexes = indexes.subList(0, i + 1);
                    String indexesAsText = currentIndexes.stream()
                                                         .map(Object::toString)
                                                         .reduce(EMPTY_STRING, (oldText, index) -> oldText + ARRAY_START_SIGN + index + ARRAY_END_SIGN);
                    throw new TMobileCannotOverrideFieldException(currentPathMetaData.getCurrentFullPathWithoutIndexes() + indexesAsText, element, currentPathMetaData.getOriginalPropertyKey());
                }
            }
        }
        throw new UnsupportedOperationException("cannot return expected object for " + currentPathMetaData.getCurrentFullPath() + " " + currentPathMetaData.getPropertyArrayHelper().getDimensionalIndexes());
    }

    public boolean existElementByGivenIndex(int index) {
        return getElement(index) != null;
    }

    private void rewriteArrayWhenIsFull(int index) {
        if(indexHigherThanArraySize(index)) {
            int predictedNewSize = elements.length + INIT_SIZE;
            int newSize = predictedNewSize > index ? predictedNewSize : index + 1;
            TMobileAbstractJsonType[] elementsTemp = new TMobileAbstractJsonType[newSize];
            System.arraycopy(elements, 0, elementsTemp, 0, elements.length);
            elements = elementsTemp;
        }
    }

    private boolean indexHigherThanArraySize(int index) {
        return index > getLastIndex(elements);
    }

    public TMobileAbstractJsonType getElement(int index) {
        rewriteArrayWhenIsFull(index);
        return elements[index];
    }

    @Override
    public String toStringJson() {
        StringBuilder result = new StringBuilder().append(ARRAY_START_SIGN);
        int index = 0;
        List<TMobileAbstractJsonType> elementsAsList = convertToListWithoutRealNull();
        int lastIndex = getLastIndex(elementsAsList);
        for(TMobileAbstractJsonType element : elementsAsList) {
            if(!(element instanceof TMobileSkipJsonField)) {
                String lastSign = index == lastIndex ? EMPTY_STRING : NEW_LINE_SIGN;
                result.append(element.toStringJson())
                      .append(lastSign);
            }
            index++;
        }
        return result.append(ARRAY_END_SIGN).toString();
    }

    public List<TMobileAbstractJsonType> convertToListWithoutRealNull() {
        List<TMobileAbstractJsonType> elementsList = new ArrayList<>();

        for(int i = 0; i < maxIndex + 1; i++) {
            TMobileAbstractJsonType element = elements[i];
            if(element != null) {
                elementsList.add(element);
            } else {
                elementsList.add(NULL_OBJECT);
            }
        }
        return elementsList;
    }

    private List<TMobileAbstractJsonType> convertToListWithRealNull() {
        List<TMobileAbstractJsonType> elementsList = new ArrayList<>();
        for(int i = 0; i < maxIndex + 1; i++) {
            TMobileAbstractJsonType element = elements[i];
            elementsList.add(element);
        }
        return elementsList;
    }

    @Override
    public void merge(TMobileArrayJsonType mergeWith, TMobilePathMetadata currentPathMetadata) {
        int index = 0;
        for(TMobileAbstractJsonType abstractJsonType : mergeWith.convertToListWithRealNull()) {
            addElement(index, abstractJsonType, currentPathMetadata);
            index++;
        }
    }
}
