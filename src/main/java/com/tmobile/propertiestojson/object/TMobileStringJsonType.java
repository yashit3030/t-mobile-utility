package com.tmobile.propertiestojson.object;

import com.tmobile.propertiestojson.util.TMobileStringToJsonStringWrapper;
;

public class TMobileStringJsonType extends TMobilePrimitiveJsonType<String> {

    public TMobileStringJsonType(String value) {
        super(value);
    }

    @Override
    public String toStringJson() {
        return TMobileStringToJsonStringWrapper.wrap(value);
    }
}
