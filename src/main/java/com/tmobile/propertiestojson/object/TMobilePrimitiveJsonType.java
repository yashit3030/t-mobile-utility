package com.tmobile.propertiestojson.object;


public abstract class TMobilePrimitiveJsonType<T> extends TMobileAbstractJsonType {

    protected T value;

    public TMobilePrimitiveJsonType(T value){
        this.value = value;
    }

    @Override
    public String toStringJson() {
        return value.toString();
    }
}
