package com.tmobile.propertiestojson.object;


/**
 * Dummy object for notify that field with that value will not be added to json. Will not go to next resolver or converter.
 * It can be returned by {@link ObjectToJsonTypeConverter#convertToJsonTypeOrEmpty(PrimitiveJsonTypesResolver, Object, String)}
 * and can be returned by {@link TextToConcreteObjectResolver#returnObjectWhenCanBeResolved(PrimitiveJsonTypesResolver, String, String)}
 */
public final class TMobileSkipJsonField extends TMobileAbstractJsonType {

    public static final TMobileSkipJsonField SKIP_JSON_FIELD = new TMobileSkipJsonField();

    private TMobileSkipJsonField() {

    }

    @Override
    public String toStringJson() {
        throw new UnsupportedOperationException("This is not normal implementation of AbstractJsonType");
    }
}
