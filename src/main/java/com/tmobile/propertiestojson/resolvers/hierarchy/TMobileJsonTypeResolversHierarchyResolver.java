package com.tmobile.propertiestojson.resolvers.hierarchy;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.primitives.TMobilePrimitiveJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.primitives.adapter.TMobilePrimitiveJsonTypeResolverToNewApiAdapter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.util.exception.TMobileParsePropertiesException;

import java.util.*;

import static com.tmobile.propertiestojson.util.exception.TMobileParsePropertiesException.CANNOT_FIND_JSON_TYPE_OBJ;
import static com.tmobile.propertiestojson.util.exception.TMobileParsePropertiesException.CANNOT_FIND_TYPE_RESOLVER_MSG;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

/**
 * It looks for sufficient resolver, firstly will looks for exactly match class type provided by method {@link PrimitiveJsonTypeResolver#getClassesWhichCanResolve()}
 * if not then will looks for closets parent class or parent interface.
 * If will find resolver for parent class or parent interface at the same level, then will get parent super class as first.
 * If will find only closets super interfaces (at the same level) then will throw exception...
 */
public class TMobileJsonTypeResolversHierarchyResolver {

    private final Map<Class<?>, List<TMobileObjectToJsonTypeConverter<?>>> resolversByType = new HashMap<>();
    private final TMobileHierarchyClassResolver hierarchyClassResolver;

    public TMobileJsonTypeResolversHierarchyResolver(List<TMobileObjectToJsonTypeConverter> resolvers) {
        for(TMobileObjectToJsonTypeConverter<?> resolver : resolvers) {
            for(Class<?> canResolveType : resolver.getClassesWhichCanResolve()) {
                List<TMobileObjectToJsonTypeConverter<?>> resolversByClass = resolversByType.get(canResolveType);
                if(resolversByClass == null) {
                    List<TMobileObjectToJsonTypeConverter<?>> newResolvers = new ArrayList<>();
                    newResolvers.add(resolver);
                    resolversByType.put(canResolveType, newResolvers);
                } else {
                    resolversByClass.add(resolver);
                }
            }
        }
        List<Class<?>> typesWhichCanResolve = new ArrayList<>();
        for(TMobileObjectToJsonTypeConverter<?> resolver : resolvers) {
            typesWhichCanResolve.addAll(resolver.getClassesWhichCanResolve());
        }
        hierarchyClassResolver = new TMobileHierarchyClassResolver(typesWhichCanResolve);
    }

    public TMobileAbstractJsonType returnConcreteJsonTypeObject(TMobilePrimitiveJsonTypesResolver mainResolver,
                                                                Object instance,
                                                                String propertyKey) {
        Objects.requireNonNull(instance);
        Class<?> instanceClass = instance.getClass();
        List<TMobileObjectToJsonTypeConverter<?>> resolvers = resolversByType.get(instanceClass);
        if(resolvers == null) {
            Class<?> typeWhichCanResolve = hierarchyClassResolver.searchResolverClass(instance);
            if(typeWhichCanResolve == null) {
                throw new TMobileParsePropertiesException(format(CANNOT_FIND_TYPE_RESOLVER_MSG, instanceClass));
            }
            resolvers = resolversByType.get(typeWhichCanResolve);
        }

        if(!resolvers.isEmpty()) {
            if(instanceClass != String.class && resolvers.size() > 1 &&
               resolvers.stream().anyMatch(resolver -> resolver instanceof TMobilePrimitiveJsonTypeResolverToNewApiAdapter)) {
                List<Class<?>> resolversClasses = resolvers.stream()
                                                           .map(resolver -> {
                                                               if(resolver instanceof TMobilePrimitiveJsonTypeResolverToNewApiAdapter) {
                                                                   TMobilePrimitiveJsonTypeResolverToNewApiAdapter adapter = (TMobilePrimitiveJsonTypeResolverToNewApiAdapter) resolver;
                                                                   TMobilePrimitiveJsonTypeResolver oldImplementation = adapter.getOldImplementation();
                                                                   return oldImplementation.getClass();
                                                               }
                                                               return resolver.getClass();
                                                           }).collect(toList());
                throw new TMobileParsePropertiesException("Found: " + new ArrayList<>(resolversClasses) + " for type" + instanceClass + " expected only one!");
            }

            for(TMobileObjectToJsonTypeConverter<?> resolver : resolvers) {
                Optional<TMobileAbstractJsonType> abstractJsonType = resolver.returnOptionalJsonType(mainResolver, instance, propertyKey);
                if(abstractJsonType.isPresent()) {
                    return abstractJsonType.get();
                }
            }
        }

        throw new TMobileParsePropertiesException(format(CANNOT_FIND_JSON_TYPE_OBJ, instanceClass, propertyKey, instance));
    }
}
