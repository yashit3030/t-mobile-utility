package com.tmobile.propertiestojson.resolvers.primitives.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileArrayJsonType;
import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileBooleanToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNumberToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToBooleanResolver;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToConcreteObjectResolver;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToNumberResolver;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tmobile.propertiestojson.TMobileConstants.*;
import static com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType.NULL_OBJECT;
import static com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNullToJsonTypeConverter.NULL_TO_JSON_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.object.TMobileStringToJsonTypeConverter.STRING_TO_JSON_RESOLVER;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToNumberResolver.convertToNumber;
import static com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToStringResolver.TO_STRING_RESOLVER;


public class TMobileJsonObjectHelper {

    private static final TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver;
    private static final JsonParser jp = new JsonParser();
    private static final Gson gson = new Gson();

    static {
        List<TMobileObjectToJsonTypeConverter> toJsonResolvers = new ArrayList<>();
        toJsonResolvers.add(new TMobileNumberToJsonTypeConverter());
        toJsonResolvers.add(new TMobileBooleanToJsonTypeConverter());
        toJsonResolvers.add(STRING_TO_JSON_RESOLVER);

        List<TMobileTextToConcreteObjectResolver> toObjectsResolvers = new ArrayList<>();
        toObjectsResolvers.add(new TMobileTextToNumberResolver());
        toObjectsResolvers.add(new TMobileTextToBooleanResolver());
        toObjectsResolvers.add(TO_STRING_RESOLVER);
        primitiveJsonTypesResolver = new TMobilePrimitiveJsonTypesResolver(toObjectsResolvers, toJsonResolvers, false, NULL_TO_JSON_RESOLVER);
    }

    public static String toJson(Object object) {
        return gson.toJson(object);
    }

    public static JsonElement toJsonElement(String json) {
        return jp.parse(json);
    }

    public static TMobileObjectJsonType createObjectJsonType(JsonElement parsedJson, String propertyKey) {
        TMobileObjectJsonType objectJsonType = new TMobileObjectJsonType();
        JsonObject asJsonObject = parsedJson.getAsJsonObject();
        for(Map.Entry<String, JsonElement> entry : asJsonObject.entrySet()) {
            JsonElement someField = entry.getValue();
            TMobileAbstractJsonType valueOfNextField = convertToAbstractJsonType(someField, propertyKey);
            objectJsonType.addField(entry.getKey(), valueOfNextField, null);
        }
        return objectJsonType;
    }

    public static TMobileAbstractJsonType convertToAbstractJsonType(JsonElement someField, String propertyKey) {
        TMobileAbstractJsonType valueOfNextField = null;
        if(someField.isJsonNull()) {
            valueOfNextField = NULL_OBJECT;
        }
        if(someField.isJsonObject()) {
            valueOfNextField = createObjectJsonType(someField, propertyKey);
        }
        if(someField.isJsonArray()) {
            valueOfNextField = createArrayJsonType(someField, propertyKey);
        }
        if(someField.isJsonPrimitive()) {
            JsonPrimitive jsonPrimitive = someField.getAsJsonPrimitive();
            if(jsonPrimitive.isString()) {
                valueOfNextField = primitiveJsonTypesResolver.resolvePrimitiveTypeAndReturn(jsonPrimitive.getAsString(), propertyKey);
            } else if(jsonPrimitive.isNumber()) {
                String numberAsText = jsonPrimitive.getAsNumber().toString();
                valueOfNextField = primitiveJsonTypesResolver.resolvePrimitiveTypeAndReturn(convertToNumber(numberAsText), propertyKey);
            } else if(jsonPrimitive.isBoolean()) {
                valueOfNextField = primitiveJsonTypesResolver.resolvePrimitiveTypeAndReturn(jsonPrimitive.getAsBoolean(), propertyKey);
            }
        }
        return valueOfNextField;
    }

    public static TMobileArrayJsonType createArrayJsonType(JsonElement parsedJson, String propertyKey) {
        TMobileArrayJsonType arrayJsonType = new TMobileArrayJsonType();
        JsonArray asJsonArray = parsedJson.getAsJsonArray();
        int index = 0;
        for(JsonElement element : asJsonArray) {
            arrayJsonType.addElement(index, convertToAbstractJsonType(element, propertyKey), null);
            index++;
        }
        return arrayJsonType;
    }

    public static boolean isValidJsonObjectOrArray(String propertyValue) {
        if(hasJsonObjectSignature(propertyValue) || hasJsonArraySignature(propertyValue)) {
            JsonParser jp = new JsonParser();
            try {
                jp.parse(propertyValue);
                return true;
            } catch(Exception ex) {
                return false;
            }
        }
        return false;
    }

    public static boolean hasJsonArraySignature(String propertyValue) {
        return hasJsonSignature(propertyValue.trim(), ARRAY_START_SIGN, ARRAY_END_SIGN);
    }

    public static boolean hasJsonObjectSignature(String propertyValue) {
        return hasJsonSignature(propertyValue.trim(), JSON_OBJECT_START, JSON_OBJECT_END);
    }

    private static boolean hasJsonSignature(String propertyValue, String startSign, String endSign) {
        return firsLetter(propertyValue).contains(startSign) && lastLetter(propertyValue).contains(endSign);
    }

    private static String firsLetter(String text) {
        return text.substring(0, 1);
    }

    private static String lastLetter(String text) {
        return text.substring(text.length() - 1);
    }
}
