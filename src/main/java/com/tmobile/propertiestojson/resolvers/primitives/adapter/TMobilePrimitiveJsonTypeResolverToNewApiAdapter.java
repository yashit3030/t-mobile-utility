package com.tmobile.propertiestojson.resolvers.primitives.adapter;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.primitives.TMobilePrimitiveJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToConcreteObjectResolver;
import com.tmobile.propertiestojson.util.TMobileReflectionUtils;

import static com.tmobile.propertiestojson.util.TMobileReflectionUtils.invokeMethod;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;


@SuppressWarnings("unchecked")
public final class TMobilePrimitiveJsonTypeResolverToNewApiAdapter extends TMobilePrimitiveJsonTypeResolver
        implements TMobileTextToConcreteObjectResolver, TMobileObjectToJsonTypeConverter {

    private final TMobilePrimitiveJsonTypeResolver oldImplementation;

    public TMobilePrimitiveJsonTypeResolverToNewApiAdapter(TMobilePrimitiveJsonTypeResolver oldImplementation) {
        this.oldImplementation = oldImplementation;
        TMobileReflectionUtils.setValue(this, "canResolveClass", resolveTypeOfResolver());
    }

    @Override // from PrimitiveJsonTypeResolver and ObjectToJsonTypeConverter
    public Class<?> resolveTypeOfResolver() {
        if (oldImplementation != null) {
            return oldImplementation.resolveTypeOfResolver();
        }
        return null;
    }

    @Override // from PrimitiveJsonTypeResolver and ObjectToJsonTypeConverter
    public TMobileAbstractJsonType returnJsonType(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                  Object propertyValue,
                                                  String propertyKey) {
        return oldImplementation.returnJsonType(primitiveJsonTypesResolver,
                                                propertyValue,
                                                propertyKey);
    }

    @Override // from PrimitiveJsonTypeResolver
    protected Optional<Object> returnConcreteValueWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                    String propertyValue,
                                                                    String propertyKey) {
        return invokeMethod(oldImplementation, "returnConcreteValueWhenCanBeResolved",
                                            asList(TMobilePrimitiveJsonTypesResolver.class, String.class, String.class),
                                            asList(primitiveJsonTypesResolver, propertyValue, propertyKey));
    }

    @Override // from PrimitiveJsonTypeResolver
    public TMobileAbstractJsonType returnConcreteJsonType(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                   Object convertedValue,
                                                   String propertyKey) {
        return oldImplementation.returnConcreteJsonType(primitiveJsonTypesResolver, convertedValue, propertyKey);
    }


    @Override // from TextToConcreteObjectResolver and PrimitiveJsonTypeResolver
    public Optional<Object> returnConvertedValueForClearedText(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                               String propertyValue,
                                                               String propertyKey) {
        Optional<?> optional = oldImplementation.returnConvertedValueForClearedText(primitiveJsonTypesResolver, propertyValue, propertyKey);
        return Optional.ofNullable(optional.orElse(null));
    }

    @Override // from TextToConcreteObjectResolver
    public Optional<Object> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                          String propertyValue,
                                                          String propertyKey) {
        Optional<?> optional = returnConcreteValueWhenCanBeResolved(primitiveJsonTypesResolver, propertyValue, propertyKey);
        return Optional.ofNullable(optional.orElse(null));
    }

    @Override // from ObjectToJsonTypeConverter
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object convertedValue, String propertyKey) {
        return Optional.of(oldImplementation.returnJsonType(primitiveJsonTypesResolver, convertedValue, propertyKey));
    }

    @Override // from ObjectToJsonTypeConverter and PrimitiveJsonTypeResolver
    public List<Class<?>> getClassesWhichCanResolve() {
        return oldImplementation.getClassesWhichCanResolve();
    }

    public TMobilePrimitiveJsonTypeResolver getOldImplementation() {
        return oldImplementation;
    }
}
