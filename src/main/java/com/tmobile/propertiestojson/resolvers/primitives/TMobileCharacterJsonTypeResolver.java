package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileCharacterToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToCharacterResolver;

@Deprecated
public class TMobileCharacterJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<Character> {

    public TMobileCharacterJsonTypeResolver() {
        super(new TMobileTextToCharacterResolver(), new TMobileCharacterToJsonTypeConverter());
    }
}