package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileSuperObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToObjectResolver;
import com.tmobile.propertiestojson.resolvers.primitives.utils.TMobileJsonObjectHelper;

/**
 * When given text contains parsable json value, json object or json array then try build instance of ObjectJsonType or ArrayJsonType
 * It will invoke {@link #returnConcreteJsonType(TMobilePrimitiveJsonTypesResolver, Object, String)} after conversion from string (raw property value to some object)
 * This Resolver will convert number in json as number, text as text, boolean as boolean...
 * It uses independent, own list of json type resolvers.
 * The setup of resolvers in {@link TMobilePropertiesToJsonConverter#PropertiesToJsonConverter(PrimitiveJsonTypeResolver... primitiveResolvers)} will not have impact of those list.
 */
@Deprecated
public class TMobileObjectFromTextJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<Object> {

    public TMobileObjectFromTextJsonTypeResolver() {
        super(new TMobileTextToObjectResolver(), new TMobileSuperObjectToJsonTypeConverter());
    }


    /**
     * It convert to implementation of AbstractJsonType through use of json for conversion from java object to raw json,
     * then raw json convert to com.google.gson.JsonElement, and this JsonElement to instance of AbstractJsonType (json object, array json, or simple text json)
     *
     * @param propertyValue java bean to convert to instance of AbstractJsonType.
     * @param propertyKey   currently processed propertyKey from properties.
     * @return instance of AbstractJsonType
     */
    public static TMobileAbstractJsonType convertFromObjectToJson(Object propertyValue, String propertyKey) {
        return TMobileSuperObjectToJsonTypeConverter.convertFromObjectToJson(propertyValue, propertyKey);
    }

    public static boolean isValidJsonObjectOrArray(String propertyValue) {
        return TMobileJsonObjectHelper.isValidJsonObjectOrArray(propertyValue);
    }

    private static boolean hasJsonObjectSignature(String propertyValue) {
        return TMobileJsonObjectHelper.hasJsonObjectSignature(propertyValue);
    }
}
