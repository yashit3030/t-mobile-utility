package com.tmobile.propertiestojson.resolvers.primitives.delegator;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.primitives.TMobilePrimitiveJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileAbstractObjectToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToConcreteObjectResolver;
import com.tmobile.propertiestojson.util.TMobileReflectionUtils;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unchecked")
public class TMobilePrimitiveJsonTypeDelegatorResolver<T> extends TMobilePrimitiveJsonTypeResolver<T> {

    private final TMobileTextToConcreteObjectResolver toObjectResolver;
    private final TMobileAbstractObjectToJsonTypeConverter toJsonResolver;

    public TMobilePrimitiveJsonTypeDelegatorResolver(TMobileTextToConcreteObjectResolver toObjectResolver,
                                                     TMobileAbstractObjectToJsonTypeConverter toJsonResolver) {
        this.toObjectResolver = toObjectResolver;
        this.toJsonResolver = toJsonResolver;
        TMobileReflectionUtils.setValue(this, "canResolveClass", resolveTypeOfResolver());
    }

    @Override
    public Class<?> resolveTypeOfResolver() {
        if (toJsonResolver != null) {
            return toJsonResolver.resolveTypeOfResolver();
        }
        return null;
    }

    @Override
    public TMobileAbstractJsonType returnConcreteJsonType(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                          T convertedValue,
                                                          String propertyKey) {
        Optional<TMobileAbstractJsonType> optional = toJsonResolver.convertToJsonTypeOrEmpty(primitiveJsonTypesResolver,
                                                                                      convertedValue,
                                                                                      propertyKey);
        return optional.get();
    }

    @Override
    protected Optional<T> returnConcreteValueWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                               String propertyValue,
                                                               String propertyKey) {
        Optional<Object> optionalObject = toObjectResolver.returnObjectWhenCanBeResolved(primitiveJsonTypesResolver,
                                                                            propertyValue, propertyKey);
        return optionalObject.map(o -> (T) o);
    }

    @Override
    public List<Class<?>> getClassesWhichCanResolve() {
        return toJsonResolver.getClassesWhichCanResolve();
    }
}
