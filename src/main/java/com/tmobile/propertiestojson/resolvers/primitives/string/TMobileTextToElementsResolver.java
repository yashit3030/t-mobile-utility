package com.tmobile.propertiestojson.resolvers.primitives.string;



import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.tmobile.propertiestojson.TMobileConstants.*;
import static com.tmobile.propertiestojson.resolvers.primitives.utils.TMobileJsonObjectHelper.hasJsonArraySignature;
import static com.tmobile.propertiestojson.resolvers.primitives.utils.TMobileJsonObjectHelper.isValidJsonObjectOrArray;
import static java.lang.String.join;

public class TMobileTextToElementsResolver implements TMobileTextToConcreteObjectResolver<List<?>> {

    private final String arrayElementSeparator;
    private final boolean resolveTypeOfEachElement;

    public TMobileTextToElementsResolver() {
        this(true);
    }

    public TMobileTextToElementsResolver(boolean resolveTypeOfEachElement) {
        this(resolveTypeOfEachElement, SIMPLE_ARRAY_DELIMITER);
    }

    public TMobileTextToElementsResolver(boolean resolveTypeOfEachElement, String arrayElementSeparator) {
        this.resolveTypeOfEachElement = resolveTypeOfEachElement;
        this.arrayElementSeparator = arrayElementSeparator;
    }

    @Override
    public Optional<List<?>> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey) {
        if(isSimpleArray(propertyValue) && !isValidJsonObjectOrArray(propertyValue)) {

            if(hasJsonArraySignature(propertyValue)) {
                propertyValue = propertyValue
                        .replaceAll("]\\s*$", EMPTY_STRING)
                        .replaceAll("^\\s*\\[\\s*", EMPTY_STRING);
                String[] elements = propertyValue.split(arrayElementSeparator);
                List<String> clearedElements = new ArrayList<>();
                for(String element : elements) {
                    element = element.trim();
                    clearedElements.add(element);
                }
                propertyValue = join(arrayElementSeparator, clearedElements);
            }

            List<Object> elements = new ArrayList<>();
            for(String element : propertyValue.split(arrayElementSeparator)) {
                if(resolveTypeOfEachElement) {
                    elements.add(primitiveJsonTypesResolver.getResolvedObject(element, propertyKey));
                } else {
                    elements.add(element);
                }
            }
            return Optional.of(elements);
        }
        return Optional.empty();
    }

    private boolean isSimpleArray(String propertyValue) {
        return propertyValue.contains(arrayElementSeparator) || hasJsonArraySignature(propertyValue);
    }
}
