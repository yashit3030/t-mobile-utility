package com.tmobile.propertiestojson.resolvers.primitives.object;

import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileArrayJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;

public class TMobileElementsToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<Collection<?>> {

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, Collection<?> elements, String propertyKey) {
        return Optional.of(new TMobileArrayJsonType(primitiveJsonTypesResolver, elements, null, propertyKey));
    }

    @Override
    public Optional<TMobileAbstractJsonType> returnOptionalJsonType(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, Object propertyValue, String propertyKey) {
        if(hasElements(propertyValue.getClass())) {
            Collection<?> collection;
            if (propertyValue.getClass().isArray()) {
                Object[] rawArray = (Object[]) propertyValue;
                collection  = new ArrayList<>(Arrays.asList(rawArray));
            } else {
                collection = (Collection<?>) propertyValue;
            }
            return convertToJsonTypeOrEmpty(primitiveJsonTypesResolver, collection, propertyKey);
        }
        return convertToJsonTypeOrEmpty(primitiveJsonTypesResolver, singletonList(propertyValue), propertyKey);
    }

    public boolean hasElements(Class<?> classToTest) {
        return canResolveClass.isAssignableFrom(classToTest) || classToTest.isArray();
    }

    @Override
    public Class<?> resolveTypeOfResolver() {
        return Collection.class;
    }

    @Override
    public List<Class<?>> getClassesWhichCanResolve() {
        return Arrays.asList(Collection.class, Object[].class);
    }
}
