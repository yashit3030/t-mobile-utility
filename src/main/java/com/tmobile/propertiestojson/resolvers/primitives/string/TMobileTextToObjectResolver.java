package com.tmobile.propertiestojson.resolvers.primitives.string;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

import static com.tmobile.propertiestojson.resolvers.primitives.utils.TMobileJsonObjectHelper.*;


public class TMobileTextToObjectResolver implements TMobileTextToConcreteObjectResolver<TMobileAbstractJsonType> {

    @Override
    public Optional<TMobileAbstractJsonType> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey) {
        if(hasJsonObjectSignature(propertyValue) || hasJsonArraySignature(propertyValue)) {
            try {
                return Optional.ofNullable(convertToAbstractJsonType(toJsonElement(propertyValue), propertyKey));
            } catch(Exception exception) {
                return Optional.empty();
            }
        }
        return Optional.empty();
    }
}
