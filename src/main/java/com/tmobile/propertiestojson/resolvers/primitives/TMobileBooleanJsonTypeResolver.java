package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileBooleanToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToBooleanResolver;

@Deprecated
public class TMobileBooleanJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<Boolean> {

    public TMobileBooleanJsonTypeResolver() {
        super(new TMobileTextToBooleanResolver(), new TMobileBooleanToJsonTypeConverter());
    }
}
