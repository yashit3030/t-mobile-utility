package com.tmobile.propertiestojson.resolvers.primitives;


import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileNumberToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToNumberResolver;

@Deprecated
public class TMobileNumberJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<Number> {

    public TMobileNumberJsonTypeResolver() {
        super(new TMobileTextToNumberResolver(), new TMobileNumberToJsonTypeConverter());
    }
}
