package com.tmobile.propertiestojson.resolvers.primitives;

import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobilePrimitiveJsonTypeDelegatorResolver;
import com.tmobile.propertiestojson.resolvers.primitives.object.TMobileElementsToJsonTypeConverter;
import com.tmobile.propertiestojson.resolvers.primitives.string.TMobileTextToElementsResolver;

import java.util.Collection;

/**
 * When given text contains ',' or text starts with '[' and ends with ']' in text then it tries split by comma and remove '[]' signs and then
 * every separated text tries convert to json value.
 *
 */
@Deprecated
public class TMobilePrimitiveArrayJsonTypeResolver extends TMobilePrimitiveJsonTypeDelegatorResolver<Collection<?>> {

    public TMobilePrimitiveArrayJsonTypeResolver() {
        super(new TMobileTextToElementsResolver(), new TMobileElementsToJsonTypeConverter());
    }

    public TMobilePrimitiveArrayJsonTypeResolver(boolean resolveTypeOfEachElement) {
        super(new TMobileTextToElementsResolver(resolveTypeOfEachElement), new TMobileElementsToJsonTypeConverter());
    }
}
