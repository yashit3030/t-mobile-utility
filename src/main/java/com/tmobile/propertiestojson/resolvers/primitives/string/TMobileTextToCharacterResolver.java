package com.tmobile.propertiestojson.resolvers.primitives.string;



import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileTextToCharacterResolver implements TMobileTextToConcreteObjectResolver<Character> {

    @Override
    public Optional<Character> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver, String propertyValue, String propertyKey) {
        if(propertyValue.length() == 1) {
            return Optional.of(propertyValue.charAt(0));
        }
        return Optional.empty();
    }
}
