package com.tmobile.propertiestojson.resolvers.primitives.object;

import com.google.gson.JsonElement;
import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileStringJsonType;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

import static com.tmobile.propertiestojson.object.TMobileJsonNullReferenceType.NULL_OBJECT;
import static com.tmobile.propertiestojson.resolvers.primitives.utils.TMobileJsonObjectHelper.*;


public class TMobileSuperObjectToJsonTypeConverter extends TMobileAbstractObjectToJsonTypeConverter<Object> {

    /**
     * It return instance of ArrayJsonType or ObjectJsonType
     * if propertyValue is not one of above types then will convert it to gson JsonElement instance and then convert to ArrayJsonType, ObjectJsonType, StringJsonType
     *
     * @param primitiveJsonTypesResolver resolver which is main resolver for leaf value from property
     * @param propertyValue              property key.
     * @return instance of ArrayJsonType, ObjectJsonType or StringJsonType
     */

    @Override
    public Optional<TMobileAbstractJsonType> convertToJsonTypeOrEmpty(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                                      Object propertyValue,
                                                                      String propertyKey) {
        if(TMobileAbstractJsonType.class.isAssignableFrom(propertyValue.getClass())) {
            return Optional.of((TMobileAbstractJsonType) propertyValue);
        }
        return Optional.of(convertFromObjectToJson(propertyValue, propertyKey));
    }

    /**
     * It convert to implementation of AbstractJsonType through use of json for conversion from java object to raw json,
     * then raw json convert to com.google.gson.JsonElement, and this JsonElement to instance of AbstractJsonType (json object, array json, or simple text json)
     *
     * @param propertyValue java bean to convert to instance of AbstractJsonType.
     * @param propertyKey   currently processed propertyKey from properties.
     * @return instance of AbstractJsonType
     */
    public static TMobileAbstractJsonType convertFromObjectToJson(Object propertyValue, String propertyKey) {
        return convertToObjectArrayOrJsonText(toJsonElement(toJson(propertyValue)), propertyKey);
    }

    private static TMobileAbstractJsonType convertToObjectArrayOrJsonText(JsonElement someField, String propertyKey) {
        TMobileAbstractJsonType valueOfNextField = null;
        if(someField.isJsonNull()) {
            valueOfNextField = NULL_OBJECT;
        }
        if(someField.isJsonObject()) {
            valueOfNextField = createObjectJsonType(someField, propertyKey);
        }
        if(someField.isJsonArray()) {
            valueOfNextField = createArrayJsonType(someField, propertyKey);
        }
        if(someField.isJsonPrimitive()) {
            return new TMobileStringJsonType(someField.toString());
        }
        return valueOfNextField;
    }
}
