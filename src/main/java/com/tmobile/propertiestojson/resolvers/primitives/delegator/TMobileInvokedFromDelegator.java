package com.tmobile.propertiestojson.resolvers.primitives.delegator;

/**
 * Invoked methods from old resolvers, which delegate methods call to new interfaces.
 */
public @interface TMobileInvokedFromDelegator {
}
