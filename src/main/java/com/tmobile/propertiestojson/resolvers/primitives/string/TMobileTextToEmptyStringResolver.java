package com.tmobile.propertiestojson.resolvers.primitives.string;

import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;

import java.util.Optional;

public class TMobileTextToEmptyStringResolver implements TMobileTextToConcreteObjectResolver<String> {

    public static final TMobileTextToEmptyStringResolver EMPTY_TEXT_RESOLVER = new TMobileTextToEmptyStringResolver();
    private final static String EMPTY_VALUE = "";

    @Override
    public Optional<String> returnObjectWhenCanBeResolved(TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver,
                                                          String propertyValue,
                                                          String propertyKey) {
        String text = propertyValue.equals(EMPTY_VALUE) ? EMPTY_VALUE : null;
        return Optional.ofNullable(text);
    }
}
