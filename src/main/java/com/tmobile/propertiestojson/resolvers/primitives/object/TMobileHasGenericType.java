package com.tmobile.propertiestojson.resolvers.primitives.object;


import com.tmobile.propertiestojson.resolvers.primitives.adapter.TMobileInvokedFromAdapter;
import com.tmobile.propertiestojson.resolvers.primitives.delegator.TMobileInvokedFromDelegator;
import com.tmobile.propertiestojson.util.exception.TMobileParsePropertiesException;

import java.lang.reflect.ParameterizedType;

public interface TMobileHasGenericType<T> {

    @SuppressWarnings("unchecked")
    @TMobileInvokedFromAdapter
    @TMobileInvokedFromDelegator
    default Class<?> resolveTypeOfResolver() {
        Class<?> currentClass = getClass();
        while(currentClass != null) {
            try {
                return (Class<T>) ((ParameterizedType) currentClass
                        .getGenericSuperclass()).getActualTypeArguments()[0];
            } catch(Exception ccx) {
                currentClass = currentClass.getSuperclass();
            }
        }
        throw new TMobileParsePropertiesException("Cannot find generic type for resolver: " + getClass() + " You can resolve it by one of below:"
                                           + "\n 1. override method resolveTypeOfResolver() for provide explicit class type " +
                                           "\n 2. add generic type during extension of PrimitiveJsonTypeResolver "
                                           + "'class " + getClass().getSimpleName() + " extends PrimitiveJsonTypeResolver<GIVEN_TYPE>'");
    }
}
