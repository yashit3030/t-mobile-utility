package com.tmobile.propertiestojson;



import com.tmobile.propertiestojson.object.TMobileAbstractJsonType;
import com.tmobile.propertiestojson.object.TMobileObjectJsonType;
import com.tmobile.propertiestojson.object.TMobileSkipJsonField;
import com.tmobile.propertiestojson.path.TMobilePathMetadata;
import com.tmobile.propertiestojson.resolvers.TMobileJsonTypeResolver;
import com.tmobile.propertiestojson.resolvers.TMobilePrimitiveJsonTypesResolver;
import com.tmobile.propertiestojson.resolvers.transfer.TMobileDataForResolve;

import java.util.Map;

public class TMobileJsonObjectsTraverseResolver {

    private final Map<TMobileAlgorithmType, TMobileJsonTypeResolver> algorithms;
    private final TMobilePrimitiveJsonTypesResolver primitiveJsonTypesResolver;
    private Map<String, String> properties;
    private String propertyKey;
    private TMobilePathMetadata rootPathMetaData;
    private TMobileObjectJsonType currentObjectJsonType;

    public TMobileJsonObjectsTraverseResolver(Map<TMobileAlgorithmType, TMobileJsonTypeResolver> algorithms,
                                       Map<String, String> properties, String propertyKey,
                                              TMobilePathMetadata rootPathMetaData, TMobileObjectJsonType coreObjectJsonType) {
        this.properties = properties;
        this.propertyKey = propertyKey;
        this.rootPathMetaData = rootPathMetaData;
        this.currentObjectJsonType = coreObjectJsonType;
        this.algorithms = algorithms;
        this.primitiveJsonTypesResolver = (TMobilePrimitiveJsonTypesResolver) algorithms.get(TMobileAlgorithmType.PRIMITIVE);
    }

    public void initializeFieldsInJson() {
        TMobilePathMetadata currentPathMetaData = rootPathMetaData;
        Object valueFromProperties = properties.get(currentPathMetaData.getOriginalPropertyKey());
        if(valueFromProperties != null) {
            if(valueFromProperties instanceof TMobileSkipJsonField) {
                return;
            }
        }
        TMobileAbstractJsonType resolverJsonObject = primitiveJsonTypesResolver.resolvePrimitiveTypeAndReturn(valueFromProperties, currentPathMetaData.getOriginalPropertyKey());
        if (resolverJsonObject instanceof TMobileSkipJsonField && !rootPathMetaData.getLeaf().isArrayField()) {
            return;
        } else {
            rootPathMetaData.getLeaf().setJsonValue(resolverJsonObject);
        }
        rootPathMetaData.getLeaf().setRawValue(properties.get(propertyKey));

        while(currentPathMetaData != null) {
            TMobileDataForResolve dataForResolve = new TMobileDataForResolve(properties, propertyKey, currentObjectJsonType, currentPathMetaData);
            currentObjectJsonType = algorithms.get(resolveAlgorithm(currentPathMetaData))
                                              .traverseOnObjectAndInitByField(dataForResolve);
            currentPathMetaData = currentPathMetaData.getChild();
        }
    }

    private TMobileAlgorithmType resolveAlgorithm(TMobilePathMetadata currentPathMetaData) {
        if(isPrimitiveField(currentPathMetaData)) {
            return TMobileAlgorithmType.PRIMITIVE;
        }
        if(currentPathMetaData.isArrayField()) {
            return TMobileAlgorithmType.ARRAY;
        }
        return TMobileAlgorithmType.OBJECT;
    }


    private boolean isPrimitiveField(TMobilePathMetadata currentPathMetaData) {
        return currentPathMetaData.isLeaf();
    }

}
