package com.tmobile.parser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class TMobileFileParserApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(TMobileFileParserApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TMobileFileParserApplication.class);
    }
}