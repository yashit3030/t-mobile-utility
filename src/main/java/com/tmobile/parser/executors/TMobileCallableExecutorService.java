package com.tmobile.parser.executors;

import com.tmobile.parser.model.HAProxyConfigModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

public class TMobileCallableExecutorService implements Callable<List<String>> {
    List<String> duplicateList;
    List<HAProxyConfigModel> propList;

    public TMobileCallableExecutorService(List<String> duplicateList, List<HAProxyConfigModel> propList) {
        this.duplicateList = duplicateList;
        this.propList = propList;
    }
    @Override
    public List<String> call() {
        List<String> dList = new ArrayList<>();
        System.out.println("Duplicate List Size :"+ duplicateList.size() );
        for (String i1 : duplicateList) {
            for (int i = 0; i < propList.size(); i++) {
                if (i1.equals(propList.get(i).getKey())) {
                    dList.add(i1 + ":" + i);
                }
            }
        }
        return dList;
    }
}
