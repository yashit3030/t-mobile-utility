package com.tmobile.parser.executors;

import com.tmobile.parser.model.HAProxyConfigModel;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.*;
@Service
public class TMobileCallableHelper {

    public List<String> executeCallable(Set<String> duplicateList, List<HAProxyConfigModel> propList){
        List<String> dList = new ArrayList<>();
        List<String> mainList = new ArrayList<String>();
        mainList.addAll(duplicateList);
        List<List<String>> parts = new ArrayList<>();
        int L = 20;
        final int N = mainList.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<String>(mainList.subList(i, Math.min(N, i + L))));
        }
        System.out.println(parts.size());
        ExecutorService executor  = Executors.newFixedThreadPool(15);
        List<TMobileCallableExecutorService> taskList = new LinkedList<>();
        for (List<String> partialKeysList : parts) {
            TMobileCallableExecutorService task = new TMobileCallableExecutorService(partialKeysList, propList);
            taskList.add(task);
        }
        Map<String, List<Integer>> dupKeysIndex = new LinkedHashMap<>();
        //Execute all tasks and get reference to Future objects
        List<Future<List<String>>> dupList = null;
        try {
            dupList = executor.invokeAll(taskList);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executor.shutdown();

        System.out.println("\n========Printing the results======");

        for (int i = 0; i < dupList.size(); i++) {
            Future<List<String>> future = dupList.get(i);
            try {
                List<String> result = future.get();
                dList.addAll(result);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return dList;
    }

}
