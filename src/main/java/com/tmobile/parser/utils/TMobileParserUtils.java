package com.tmobile.parser.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.tmobile.parser.executors.TMobileCallableHelper;
import com.tmobile.propertiestojson.util.TMobilePropertiesToJsonConverter;
import com.tmobile.parser.model.BackEndConfigModel;
import com.tmobile.parser.model.HAProxyConfigModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URLDecoder;
import java.util.*;

import static com.tmobile.parser.utils.TMobileConstants.*;
@Service
@SuppressWarnings({"unchecked", "unused", "MismatchedQueryAndUpdateOfStringBuilder"})
public class TMobileParserUtils {
    @Autowired
    TMobileCallableHelper helper;

    public List<HAProxyConfigModel> parseConfig(String inputFileName) throws Exception {
        // Initialize Objects
        Map<String, List<HAProxyConfigModel>> conMap = new LinkedHashMap<>();
        HAProxyConfigModel fc = null;
        List<HAProxyConfigModel> fcList = new ArrayList<>();
        HAProxyConfigModel prop = new HAProxyConfigModel();
        List<HAProxyConfigModel> propList = new LinkedList<>();
        File file = new File(inputFileName);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String origLine;
        List<String> fileStr = new ArrayList<>();
        Set<String> mainElements = new LinkedHashSet<>();
        //'global', 'resolvers', 'defaults', 'listen', 'frontend', 'backend'
        String[] configAttributes = HAPROXY_SECTIONS;
        while ((origLine = br.readLine()) != null) {
            if (origLine.length() == origLine.trim().length() && StringUtils.isNotEmpty(origLine.trim()) &&
                    !(origLine.startsWith("#") || origLine.contains("#"))) {
                if(origLine.trim().startsWith(BACK_END_CONFIG))
                    mainElements.add(BACK_END_CONFIG);
                else
                    mainElements.add(origLine);
            }
            fileStr.add(origLine);
        }
        List<String> validHeadList = new ArrayList<>();
        for (String head : mainElements) {
            String validHead = head.split(WHITE_SPACE)[0];
            for (String attrs : configAttributes) {
                if (attrs.equalsIgnoreCase(validHead)) {
                    validHeadList.add(head);
                    break;
                }
            }
        }
        int backEndIndex = 0;
        int mainIndex = 0;
        int aclSubIndex = 0;
        int useSubIndex = 0;
        String rootElement = null;
        boolean isNotAclFlag = true;
        boolean isNotBackEndFlag = true;
        List<BackEndConfigModel> bcList = new LinkedList<>();
        boolean aclArrayFlag = false;
        boolean useArrayFlag = false;
        int subIndex = 0;
        boolean newLineAlreadyHandled = false;
        for( int start=0; start<fileStr.size();start++){
            String line = fileStr.get(start);
            if (StringUtils.isEmpty(rootElement)) {
                rootElement = "";
            }
            if (!line.trim().startsWith(USE)
                    && (line.length() == line.trim().length() && StringUtils.isNotEmpty(line.trim())
                    && !line.trim().startsWith(HASH_COMMENT))
                    || (line.trim().startsWith(ACL) && isNotAclFlag)
                    || (line.trim().startsWith(BACK_END_CONFIG) && isNotBackEndFlag)) {
                if (propList.size() > 0 && !line.trim().startsWith(BACK_END_CONFIG) && !rootElement.equalsIgnoreCase(BACK_END_CONFIG)) {
                    conMap.put(rootElement, propList);
                    prop = null;
                    propList = new LinkedList<>();
                    subIndex = 0;
                    aclArrayFlag = false;
                    useArrayFlag = false;
                }
                rootElement = line;
                if (!validHeadList.contains(rootElement)) {
                    rootElement = line.trim().split(WHITE_SPACE)[0];
                }
                if (rootElement.equalsIgnoreCase(ACL)) {
                    isNotAclFlag = false;
                }

            }
            if (StringUtils.isNotEmpty(rootElement)) {
                if (prop == null) {
                    prop = new HAProxyConfigModel();
                }
                if (StringUtils.isNotEmpty(line.trim()) && !line.equalsIgnoreCase(rootElement) && !line.contains(HASH_COMMENT)) {
                    if (rootElement.equalsIgnoreCase(ACL)) {
                        String prevAcl = "";
                        if(start >0 && StringUtils.isNotEmpty(line) &&
                                !(line.trim().startsWith(HASH_COMMENT) || line.trim().startsWith(USE))){
                            if(fileStr.get(start-1).trim().startsWith(ACL)){
                                prevAcl = ACL;
                            }
                        }
                        String nextAcl = "";
                        if(start <= (fileStr.size() -2) && line.trim().startsWith(ACL)){
                            if(fileStr.get(start+1).trim().startsWith(ACL) || fileStr.get(start+2).trim().startsWith(ACL)){
                                nextAcl = ACL;
                            }

                        }
                        String prevUse = "";
                        String nextUse = "";
                        if(start >0 && StringUtils.isNotEmpty(line) && line.trim().startsWith(USE)){
                            if(fileStr.get(start-1).trim().startsWith(USE)){
                                prevUse = USE;
                            }
                        }
                        if(start <= (fileStr.size() -1) && line.trim().startsWith(USE)){
                            if(fileStr.get(start+1).trim().startsWith(USE)){
                                nextUse = USE;
                            }

                        }
                        if(StringUtils.isNotEmpty(prevAcl.trim()) || StringUtils.isNotEmpty(nextAcl.trim())){
                            if(aclArrayFlag){
                                aclSubIndex++;
                            }
                            aclArrayFlag = true;
                        }
                        else if(StringUtils.isNotEmpty(prevUse.trim()) || StringUtils.isNotEmpty(nextUse.trim())){
                            if(useArrayFlag){
                                useSubIndex++;
                            }
                            useArrayFlag = true;
                        }
                        if(line.trim().startsWith(ACL)){
                            subIndex = aclSubIndex;
                            aclArrayFlag = useArrayFlag;
                        }
                        else if(line.trim().startsWith(USE)){
                            subIndex = useSubIndex;
                            aclArrayFlag = useArrayFlag;
                        }
                        propList.add(populateAclPropertiesModel(line, mainIndex, subIndex, aclArrayFlag));
                        newLineAlreadyHandled = false;
                    }
                    else if (rootElement.equalsIgnoreCase(BACK_END_CONFIG)) {
                        propList.add(populateBackendPropertiesModel(line, backEndIndex));
                    } else {
                        propList.add(populatePropertiesModel(line));
                    }
                }
            }
            if (StringUtils.isEmpty(line.trim()) && !isNotBackEndFlag) {
                backEndIndex++;
                isNotBackEndFlag = true;
            }
            if ((StringUtils.isEmpty(line.trim()) ) && rootElement.equalsIgnoreCase(ACL) && !newLineAlreadyHandled ) {
                mainIndex++;
                isNotAclFlag = false;
                subIndex = 0;
                aclSubIndex = 0;
                useSubIndex = 0;
                aclArrayFlag = false;
                useArrayFlag = false;
                newLineAlreadyHandled = true;
            }
            if (rootElement.equalsIgnoreCase(BACK_END_CONFIG)) {
                isNotBackEndFlag = false;
            }
        } // End of for loop for reading line from a list
        // Start Parsing
        if (propList.size() > 0) {
            conMap.put(rootElement, propList);
        }
        List<HAProxyConfigModel> newList = handleWhiteSpacesInKeys(conMap, PROP_JOINER);
        br.close();
        fr.close();
        // Return list to display in UI for CRUD operations
        return newList;
    }
    //backend[0].backend=check_WLNPort.WLNPortEligibility_prd02c
    //backend[0].mode=http
    private HAProxyConfigModel populateBackendPropertiesModel(String line, int index) {
        String key, value = EMPTY_STRING;
        HAProxyConfigModel prop = new HAProxyConfigModel();
        if (StringUtils.isNotEmpty(line)) {
            String[] spilts = line.trim().split(WHITE_SPACE);
            key = line.trim().split(WHITE_SPACE)[0];
            if (spilts.length > 1) {
                value = line.trim().substring(key.length() + 1);
            }
            key = BACK_END_CONFIG + OPEN_SQUARE_BRACE + index + CLOSE_SQUARE_BRACE + DOT + key;
            prop.setKey(key.trim());
            prop.setValue(value);
        }
        return prop;
    }
    /*
    acl[0].acl[0]=MakeCreditCardPayment
    acl[0].use_backend[0]=MakeCreditCardPayment.
    acl[0].acl[1]=MakeCreditCardPayment
    acl[0].acl[2]=MakeCreditCardPayment
    acl[0].use_backend[1]=MakeCreditCardPayment.
    acl[1].acl=LocationService.
    acl[1].use_backend=LocationService
     */
    private HAProxyConfigModel populateAclPropertiesModel(String line, int mainIndex, int subIndex, boolean arrayFlag) {
        String key, value = EMPTY_STRING;
        HAProxyConfigModel prop = new HAProxyConfigModel();
        if (StringUtils.isNotEmpty(line)) {
            String[] spilts = line.trim().split(WHITE_SPACE);
            key = line.trim().split(WHITE_SPACE)[0];
            if (spilts.length > 1) {
                value = line.trim().substring(key.length() + 1);
            }
            if(arrayFlag) {
                key = ACL + OPEN_SQUARE_BRACE + mainIndex + CLOSE_SQUARE_BRACE + DOT +
                      key + OPEN_SQUARE_BRACE + subIndex + CLOSE_SQUARE_BRACE;
            }
            else {
                key = ACL + OPEN_SQUARE_BRACE + mainIndex + CLOSE_SQUARE_BRACE + DOT + key;
            }
            prop.setKey(key.trim());
            prop.setValue(value);
        }
        return prop;
    }
    private HAProxyConfigModel populatePropertiesModel(String line) {
        String key, value = EMPTY_STRING;
        HAProxyConfigModel prop = new HAProxyConfigModel();
        if (StringUtils.isNotEmpty(line)) {
            String[] spilts = line.trim().split(WHITE_SPACE);
            key = line.trim().split(WHITE_SPACE)[0];
            if (spilts.length > 1) {
                value = line.trim().substring(key.length() + 1);
            }
            prop.setKey(key.trim());
            prop.setValue(value);
        }
        return prop;
    }

    private List<HAProxyConfigModel> handleWhiteSpacesInKeys(Map<String, List<HAProxyConfigModel>> conMap,
                                                                    String delimiter) {
        List<HAProxyConfigModel> newList = new LinkedList<>();
        for (String root : conMap.keySet()) {
            List<HAProxyConfigModel> elements = conMap.get(root);
            StringBuilder newRoot = new StringBuilder();
            // Check for White spaces in Key
            String[] propRoot = root.split(WHITE_SPACE);
            for (int i = 0; i < propRoot.length; i++) {
                if (i == propRoot.length - 1)
                    newRoot.append(propRoot[i]);
                else
                    newRoot.append(propRoot[i]).append(delimiter).append(WHITE_SPACE);
            }
            for (HAProxyConfigModel con : elements) {
                HAProxyConfigModel p = new HAProxyConfigModel();
                String newKey = con.getKey();
                if (newRoot.toString().equalsIgnoreCase(con.getKey()) && newRoot.toString().equals("default_backend")) {
                    newKey = newRoot.toString();
                } else if (!newRoot.toString().equalsIgnoreCase(ACL) && !newRoot.toString().equalsIgnoreCase(BACK_END_CONFIG)) {
                    newKey = newRoot + "." + con.getKey();
                }
                p.setKey(newKey);
                p.setValue(con.getValue());
                newList.add(p);
            }
        }
        return newList;
    }

    public List<HAProxyConfigModel> constructObject(List<String> keyValues) throws Exception {
        List<HAProxyConfigModel> modelList = new LinkedList<>();
        for (String keyValue : keyValues) {
            HAProxyConfigModel model = new HAProxyConfigModel();
            String[] element = keyValue.split("\t");
            String key, value = EMPTY_STRING;
            key = element[0];
            if (element.length >= 2) {
                value = element[1];
            }
            model.setKey(URLDecoder.decode(key, "UTF-8"));
            model.setValue(URLDecoder.decode(value, "UTF-8"));
            modelList.add(model);
        }
        return modelList;
    }
    // For ACL
    // acl[0].acl=acl_healthcheck path_end -i /vertex-ws/listVertexServices.jsp
    public List<HAProxyConfigModel> constructKeys(List<HAProxyConfigModel> newList) {
        // Process newList for duplicate Keys ....
        Map<String, List<Integer>> finalDuplicateMap = prepareDuplicateMap(newList);
        Set<String> keys = finalDuplicateMap.keySet();
        for (String k : keys) {
            List<Integer> index = finalDuplicateMap.get(k);
            for (int i = 0; i < index.size(); i++) {
                int ind = index.get(i);
                HAProxyConfigModel propNew = newList.get(ind);
                String key = propNew.getKey();
                String newKey;
                if (key.startsWith(BACK_END_CONFIG + DOT)) {
                    newKey = key.split(SPLIT_DOT)[0] + OPEN_SQUARE_BRACE + i + CLOSE_SQUARE_BRACE + DOT + key.split(SPLIT_DOT)[1];
                } else {
                    newKey = propNew.getKey() + OPEN_SQUARE_BRACE + i + CLOSE_SQUARE_BRACE;
                }
                propNew.setKey(newKey);
                newList.set(ind, propNew);
            }
        }
        return newList;
    }
    private Map prepareDuplicateMap(List<HAProxyConfigModel> propList) {
        Set<String> duplicateList = new HashSet<>();
        Set<String> tempSet = new HashSet<>();
        List<String> keyList = new ArrayList<>();
        for (HAProxyConfigModel pCon : propList) {
            String key = pCon.getKey();
            keyList.add(key);
            if (!tempSet.add(key)) {
                duplicateList.add(pCon.getKey());
            }
        }
        //step1:
        long startTime = System.currentTimeMillis();
        List<String> dList = new LinkedList<>();
        for (String i1 : duplicateList) {
            for (int i = 0; i < keyList.size(); i++) {
                if (i1.equals(keyList.get(i))) {
                    dList.add(i1 + ":" + i);
                }
            }
        }
        long endTime = (System.currentTimeMillis() - startTime);
        System.out.println("++++++++++++++++ 1) Time taken for step 1 :" + endTime + " : Ms.");
        // Step2
        Map<String, List<Integer>> dupKeysIndex = new LinkedHashMap<>();
        List<Integer> n = new ArrayList<>();
        for (String element : dList) {
            String[] elements = element.split(COLON);
            if (!dupKeysIndex.containsKey(elements[0])) {
                n = new ArrayList<>();
                n.add(Integer.parseInt(elements[1]));
                dupKeysIndex.put(elements[0], n);
            } else {
                n.add(Integer.parseInt(elements[1]));
            }
        }
        return dupKeysIndex;
    }
    public Map<String, String> writeToPropertiesFile(String fileName, List<HAProxyConfigModel> newList) throws Exception {
        FileWriter writer = new FileWriter(fileName);
        int size = 1;
        // Properties Config file write
        StringBuilder filestr = new StringBuilder();
        Map<String, String> propMap = new LinkedHashMap<>();
        for (HAProxyConfigModel propConfig : newList) {
            String key=EMPTY_STRING, value = EMPTY_STRING;
            if (StringUtils.isNotEmpty(propConfig.getKey())) {
                key = propConfig.getKey();
                if (key.contains(TUNE_DOT_CHKSIZE)) {
                    key = key.replace(TUNE_DOT_CHKSIZE, TUNE_HYPHEN_CHKSIZE);
                }

            }
            if (StringUtils.isNotEmpty(propConfig.getValue())) {
                value = (propConfig.getValue());
                value = value.replaceAll(WHITE_SPACE, AT_FOR_WHITESPACE);
                value = value.replaceAll(LINE_FEED,HASH_FOR_LF );
               // value = value.replaceAll(COMMA, ASTERIX);
            }
            filestr.append(key);
            filestr.append(EQUALS).append(value).append(NEW_LINE);
            // Set to Map
            propMap.put(key, value);
        }
        String write = filestr.toString();
        writer.write(write);
        writer.close();
        System.out.println("------------- Finished Writing Properties File------");
        return propMap;
    }
    public String writeToJsonFile(String sourcePropFile, String jsonFileToWrite) throws IOException {
        FileWriter writer = new FileWriter(jsonFileToWrite);
        String jsonContent = new TMobilePropertiesToJsonConverter().convertPropertiesFromFileToJson(new File(sourcePropFile));
        // String jsonContent = new PropertiesToJsonConverter().convertPropertiesFromFileToJson(new File(sourcePropFile));
        writer.write(jsonContent);
        writer.close();
        System.out.println("------------- Finished Writing Json File------");
        return jsonContent;
    }
    public String writeToYamlFile(String jsonContent, String yamlFileToWrite) throws IOException {
        FileWriter writer = new FileWriter(yamlFileToWrite);
        JsonNode jsonNodeTree = new ObjectMapper().readTree(jsonContent);
        String yamlContent = new YAMLMapper().enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
                .enable(YAMLGenerator.Feature.INDENT_ARRAYS)
                .writeValueAsString(jsonNodeTree);
        String[] yamlArray = yamlContent.split(NEW_LINE_SPLIT);
        for (int i = 0; i < yamlArray.length; i++) {
            String temp = yamlArray[i];
            if (!temp.startsWith(WHITE_SPACE) && !temp.startsWith(YAML_START_TAG)
                    && !temp.startsWith(GLOBAL) && !temp.startsWith(BACK_END_CONFIG)) {
                yamlArray[i] = NEW_LINE + temp;
            }
            if (temp.endsWith(COLON) && temp.startsWith(WHITE_SPACE)) {
                yamlArray[i] = temp.substring(0, temp.length() - 1);
            }
            temp = temp.replaceAll(AT_FOR_WHITESPACE, WHITE_SPACE);
            temp = temp.replaceAll(HASH_FOR_LF, LINE_FEED);
           // temp = temp.replaceAll(ASTERIX, COMMA );
            yamlArray[i] = temp;
        }
        yamlContent = String.join(NEW_LINE, yamlArray);
        if (yamlContent.contains(TUNE_HYPHEN_CHKSIZE)) {
            yamlContent = yamlContent.replace(TUNE_HYPHEN_CHKSIZE, TUNE_DOT_CHKSIZE);
        }
        writer.write(yamlContent);
        writer.close();
        // Formatting purpose
        File file = new File(yamlFileToWrite);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String origLine;
        String newFormat;
        StringBuilder newYamlContent = new StringBuilder();
        while ((origLine = br.readLine()) != null) {
            newFormat = origLine.replaceAll(WHITE_SPACE, HTML_NBSP) + HTML_BR;
            newYamlContent.append(newFormat);
        }
        br.close();
        fr.close();
        return newYamlContent.toString();
    }

}
