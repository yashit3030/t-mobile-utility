package com.tmobile.parser.controllers;

import com.tmobile.parser.model.HAProxyConfigModel;
import com.tmobile.parser.service.TMobileFileParserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URLDecoder;
import java.util.*;

import static com.tmobile.parser.utils.TMobileConstants.*;

@Controller
public class TMobileFileParserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TMobileFileParserController.class);
    @Autowired
    TMobileFileParserService parserService;

    // inject via application.properties
    @Value("${welcome.message:test}")
    private String message = "TMobile File Parser";

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        return FILE_UPLOAD;
    }

    @RequestMapping("/index")
    public String showIndex(Map<String, Object> model) {
        return PARSER;
    }

    public TMobileFileParserController() {
        System.out.println("+++++++++++ Default Constructor called +++++++++++");
    }

    @RequestMapping(value = "/fileupload", method = RequestMethod.POST)
    public String parseConfigFile(@RequestParam MultipartFile file, Map<String, Object> model) throws Exception {
        List<HAProxyConfigModel> configList = parserService.parseConfigFileToProperties(file);
        model.put(PROPS, configList);
        model.put(FILE, file.getOriginalFilename());
        return RESULTS;
    }

    @RequestMapping(value = "/yaml", method = RequestMethod.POST)
    @ResponseBody
    public String generateYamlFile(@ModelAttribute(value = "values") ArrayList<String> values, Map<String, Object> model) throws Exception {
        List<String> decodedList = new LinkedList<>();
        for(String s : values){
            decodedList.add(URLDecoder.decode(s, UTF8_ENCODING).trim());
        }
        String yamlContent = parserService.convertPropertiesToYaml(decodedList);
        model.put(YAML, yamlContent);
        return yamlContent;
    }
    /*
    private List<HAProxyConfigModel> poupulateModel()
    {
        List<HAProxyConfigModel> configList = new ArrayList<>();

        HAProxyConfigModel model = new HAProxyConfigModel();
        // global
        //1
        model = new HAProxyConfigModel();
        model.setKey("global.log"); model.setValue("127.0.0.1 format rfc5424 local1");
        configList.add(model);
        //2
        model = new HAProxyConfigModel();
        model.setKey("global.log-send-hostname"); model.setValue(null);
        configList.add(model);
        //3
        model = new HAProxyConfigModel();
        model.setKey("global.pidfile"); model.setValue("/var/prod/data/tibco/HAPS/http/pid/haproxy.pid");
        configList.add(model);
        //4
        model = new HAProxyConfigModel();
        model.setKey("global.maxconn"); model.setValue("8000");
        configList.add(model);
        //5
        model = new HAProxyConfigModel();
        model.setKey("global.tune.chksize"); model.setValue("32768");
        configList.add(model);
        // resolvers dns
        //1
        model = new HAProxyConfigModel();
        model.setKey("resolvers\\ dns.nameserver"); model.setValue("dns1 10.66.3.26:53");
        configList.add(model);
        //2
        model = new HAProxyConfigModel();
        model.setKey("resolvers\\ dns.nameserver"); model.setValue("dns2 10.66.3.26:53");
        configList.add(model);
        //3
        model = new HAProxyConfigModel();
        model.setKey("resolvers\\ dns.resolve_retries"); model.setValue("3");
        configList.add(model);
        //4
        model = new HAProxyConfigModel();
        model.setKey("resolvers\\ dns.timeout"); model.setValue("retry 1s");
        configList.add(model);
        //5
        model = new HAProxyConfigModel();
        model.setKey("resolvers\\ dns.hold"); model.setValue("valid 10s");
        configList.add(model);
        // listen stats
        model = new HAProxyConfigModel();
        //1
        model.setKey("listen\\ stats.mode"); model.setValue("http");
        configList.add(model);
        //2
        model = new HAProxyConfigModel();
        model.setKey("listen\\ stats.bind"); model.setValue("*:4142");
        configList.add(model);
        //3
        model = new HAProxyConfigModel();
        model.setKey("listen\\ stats.stats"); model.setValue("admin if TRUE");
        configList.add(model);
        //4
        model = new HAProxyConfigModel();
        model.setKey("listen\\ stats.stats"); model.setValue("enable");
        configList.add(model);
        //5
        model = new HAProxyConfigModel();
        model.setKey("listen\\ stats.stats"); model.setValue("uri /");
        configList.add(model);

        // defaults
        //1
        model = new HAProxyConfigModel();
        model.setKey("defaults.mode"); model.setValue("http");
        configList.add(model);
        //2
        model = new HAProxyConfigModel();
        model.setKey("defaults.log"); model.setValue("global");
        configList.add(model);
        //3
        model = new HAProxyConfigModel();
        model.setKey("defaults.option"); model.setValue("httplog");
        configList.add(model);
        //4
        model = new HAProxyConfigModel();
        model.setKey("defaults.option"); model.setValue("http-server-close");
        configList.add(model);
        //5
        model = new HAProxyConfigModel();
        model.setKey("defaults.retries"); model.setValue("3");
        configList.add(model);

        // listen http_frontend
        //1
        model = new HAProxyConfigModel();
        model.setKey("listen\\ http_frontend.mode"); model.setValue("http");
        configList.add(model);
        //2
        model = new HAProxyConfigModel();
        model.setKey("listen\\ http_frontend.option"); model.setValue("http-buffer-request");
        configList.add(model);
        //3
        model = new HAProxyConfigModel();
        model.setKey("listen\\ http_frontend.bind"); model.setValue("*:2001");
        configList.add(model);
        //4
        model = new HAProxyConfigModel();
        model.setKey("listen\\ http_frontend.acl"); model.setValue("WLNPort.WLNPortEligibility hdr_sub(SOAPAction) \"/WLNP_PORT_ELIGIBILITY\"");
        configList.add(model);
        //5
        model = new HAProxyConfigModel();
        model.setKey("listen\\ http_frontend.use_backend"); model.setValue("WirelessRouterService.GetWirelessRouterWarranty if WirelessRouterService.GetWirelessRouterWarranty_path");
        configList.add(model);

        // others
        //1
        model = new HAProxyConfigModel();
        model.setKey("default_backend"); model.setValue("tibco_care_backend");
        configList.add(model);
        return configList;
    }
    */
}
