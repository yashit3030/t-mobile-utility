<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<html lang="en">
<head>

    <link rel="stylesheet" type="text/css"
          href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>

    <!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
    <c:url value="css/main.css" var="jstlCss"/>
    <link href="${jstlCss}" rel="stylesheet"/>

</head>

<body>
<div class="container">
    <div>
        <h1>T-Mobile File Parser</h1>
        <span id="error"></span>
        <form action="/fileupload" method="post" enctype="multipart/form-data" id="fileUploadForm">
            <p style="font-family: 'Verdana'">Choose HAProxy Config File and click 'Start'.<br> Or <br>Click 'Start' to create new file.</p>
            <p></p>
            <table><tr><td>
            <input type="file" id="file" name="file" accept=".cfg" required/>
            </td>
            </tr>
            </table>
            <p></p><p></p>
            <button type="submit" class="btn btn-success save-btn" id="btnSubmit">Start</button>
            <!-- <input type="submit" value="Parse CFG" name="parse"/> -->
        </form>
    </div>
</div>
<span id="result"></span>
<script type="text/javascript">
    $(document).ready(function () {
        $("#btnSubmit").click(function (event) {
            //stop submit the form, we will post it manually.
            event.preventDefault();
            // Get form
            var form = $('#fileUploadForm')[0];
            // Create an FormData object
            var data = new FormData(form);
            // disabled the submit button
            $("#btnSubmit").prop("disabled", true);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/fileupload",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {

                    $('#result').html(data);
                    console.log("SUCCESS : ", data);
                    $("#btnSubmit").prop("disabled", false);
                },
                error: function (e) {
                    $('#result').html(e.responseText);
                    console.log("ERROR : ", e);
                    $("#btnSubmit").prop("disabled", false);
                }
            });
        });
    });

</script>
</body>

</html>