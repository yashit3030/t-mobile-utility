0. Folder Structure

TMobile-FileParser
├───conf
|       (All Config Files)
├───docs
|	(Design Document)
└───src
    └───main
        ├───java
        │   └───com
        │       └───tmobile
        │           └───parser
        │               ├───controllers
        │               ├───exception
        │               ├───io
        │               ├───model
        │               ├───service
        │               │   └───impl
        │               └───utils
        ├───resources
        │   └───static
        │       └───css
        └───webapp
            └───WEB-INF
                └───jsp

1. Buiild the application by running, (We have to install Maven to run this command)
   mvn clean install -U

2. Start the server by running the following command (We have to install java to run this command)
   java -jar target\tmobile-parser-1.0.jar

3. When server is started, it listens on 8989 port, 
   http://localhost:8989/

4. This URL, http://localhost:8989/ will open the first page

5. Click the Choose File button to navigate any .cfg files.
   (It filters only cfg files)

6. Sample cfg files are available \conf folder

7. All the output files will be written tmobile-file folder created under the working directory(Eg:TMobile-FileParser)
